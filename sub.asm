;Windows, nasm with io64.inc from sasm
%include "io64.inc"
;constants
len EQU 256
base EQU 10
char_zero EQU 48

section .bss
    a resq len
    b resq len
    res resq len
    temp resq len * 2

section .text
global CMAIN
CMAIN:
    mov rdi, a
    call read_long
    mov rdi, b
    call read_long
    mov rdi, a
    mov rsi, b
    call sub_long
    call print_long
    xor rax, rax
    ret
    
;in rdi; rdi - adress
read_long:
    push rcx
    push rbx

    mov rbx, base
.loop:
    GET_CHAR rcx
    
    cmp rcx, 10 ; end of line
    je .end
  ;  cmp rcx, 0  ; end of input, dont work(
   ; je .end
    call mul_short
    sub rcx, char_zero
    call add_short
    jmp .loop
.end:

    pop rbx
    pop rcx
    ret

;rdi - long rbx - short
mul_short:
    push rax
    push rbx
    push rcx
    push rdx
    push rdi
    push rsi
    
    xor rsi, rsi
    mov rcx, len
    
.loop:
    mov rax, [rdi]
    mul rbx       ;mul in rdx rax
    
    add rax, rsi
    mov [rdi], rax
    
    adc rdx, 0
    mov rsi, rdx
    
    add rdi, 8
    
    loop .loop
    
    pop rsi
    pop rdi
    pop rdx
    pop rcx
    pop rbx
    pop rax
    ret

;rdi - long
;rcx - short
add_short:
    push rax
    push rdi
    push rcx
    
.loop:
    mov rax, rdi
    add rdi, 8
    add [rax], rcx
    xor rcx, rcx
    adc rcx, 0
    jnz .loop
    
    pop rcx
    pop rdi
    pop rax
    ret
    
;rdi - long
;rbx - short
;rdx - reminder
div_short:
    push rax
    push rbx
    push rcx
    lea rdi, [rdi + len * 8]
    xor rdx, rdx
    mov rcx, len
.loop:
    sub rdi, 8
    mov rax, [rdi]
    div rbx
    mov [rdi], rax
    loop .loop
    pop rcx
    pop rbx
    pop rax
    ret

;rdi - long; zf - is zero
is_zero:
    push rdi
    push rax
    push rcx
    mov rcx, len
.loop:
    mov rax, [rdi]
    sub rax, 0
    jnz .exit
    add rdi, 8
    dec rcx
    jnz .loop
.exit:
    pop rcx
    pop rax
    pop rdi
    ret
    
;rdi - long, rdi = 0 after printing
print_long:
    push rbx
    push rdx
    push rcx
    xor rcx, rcx
    mov rbx, 10
.loop:
    call div_short
    push rdx ;saving reminder
    inc rcx
    call is_zero
    jnz .loop
.wr:
    pop rbx
    PRINT_DEC 1, rbx
    dec ecx
    jnz .wr
    
    pop rcx
    pop rdx
    pop rbx
    ret

;rdi + rsi , result in rdi
add_long:
    push rax
    push rcx
    push rsi
    push rdi
    mov rcx, len
    clc
.loop:
    mov rax, [rsi]
    adc [rdi], rax
    lea rsi, [rsi + 8]
    lea rdi, [rdi + 8]
    loop .loop
    
    pop rdi
    pop rsi
    pop rcx
    pop rax
    ret
    
;rdi - rsi , result in rdi
sub_long:
    push rax
    push rcx;counter
    push rsi
    push rdi
    mov rcx, len
    lea rsi, [rsi + len * 8]
    lea rdi, [rdi + len * 8]
    clc
.loop:
    lea rdi, [rdi - 8]
    lea rsi, [rsi - 8]
    mov rax, [rsi]
    sbb [rdi], rax
    loop .loop
    
    pop rdi
    pop rsi
    pop rcx
    pop rax
    ret
;from rsi to rdi
copy:
    push rax
    push rcx
    push rsi
    mov rcx, len
    mov rdi, temp
    push rdi
.loop:
    mov rax, [rsi]
    mov [rdi], rax
    add rdi, 8
    add rsi, 8
    loop .loop
    pop rdi
    pop rsi
    pop rcx
    pop rax
    ret

;rdi * rsi result in rax

;mul_long:
;    push rbx
;    push rcx
;    push rdi
;    push rsi
;    mov rcx, len
;    mov rax, res ; result in rdi
;.loop:
;    mov rbx, [rdi]
;    cmp rbx, 0
;    jz .zero
;    push rdi
;    call copy
;    call mul_short ;rdi rbx
;    ;need in rdi rax; in rsi rdi
;    push rsi
;    
;    mov rsi, rdi
;    mov rdi, rax
;    call add_long
;    pop rsi
;    pop rdi
;.zero:
;    lea rdi, [rdi + 8]
;    mov rbx, 1
;    shl rbx, 32
;    xchg rsi, rdi
;    
;    call mul_short
;    call mul_short
;    
;    xchg rsi, rdi
;    
;    dec rcx
;    jnz .loop
;    pop rsi
;    pop rdi
;    pop rcx
;    pop rbx
;
;    ret
;    